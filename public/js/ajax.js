// Búinn til nýr module fyrir appið sem heldur svo utan um ajax aðferðina.
var App = {};

// Initializar XMLHttpRequest objectið. Tekið sér frá ajax aðferðnni svo að hægt
// sé að útfæra þetta öðruvísi annarstaðar og til að halda 'hver aðferð hefur eitt
// hlutverk' reglunni. Þessi aðferð opnar requestiið og setur optionin inn.
App.createXHR = function(url, options) {

    // Ef browserinn styður ekki XMLHttpRequest þá er skilað false til baka. Hægt væri
    // þá að hafa aðra aðferð sem styður það í annari aðferð t.d. En þar sem allir
    // nýtíma browserar styðja þetta þá er enginn tilgangur í því (fyrir utan >= IE8 og
    // þeir browserar eiga ekki skilið að láta styðja sig lengur "http://breakupwithie8.com/")
    var xhr = false;

    if (window.XMLHttpRequest) {
        xhr = new XMLHttpRequest();
    }

    if (xhr) {
        // Athugar hvort að options hafa verið sendir með, ef ekki þá er sett tómp
        // object í staðinn.
        options = options || {};
        // Default method set sem GET
        options.method = options.method || "GET";
        // Default gögn sett í núll.
        options.data = options.data || null;

        // Búinn til query strengur úr data objectinu
        if (options.data) {
            var qstring = [];
            for ( var key in options.data ) {
                qstring.push( encodeURIComponent(key) + "=" + encodeURIComponent(options.data[key]) );
            }

            options.data = qstring.join("&");
        }

        // Kemur í veg fyrir verið sé að notað cachaðar upplýsingar.
        if ( options.cache === false && options.method.toUpperCase() == "GET" ) {
            url = url + '?_=' + new Date().getTime();
        }

        // Triggerar hvað á að gera þegar readyState breytist.
        xhr.onreadystatechange = function() {
            // Þegar response kemur og hún er í lagi þá verður þetta true.
            if ( (xhr.readyState == 4) && (xhr.status == 200 || xhr.status == 304) ) {
                var contentType = xhr.getResponseHeader( 'Content-Type' );

                // Hvernig viljum við meðhöndla gögnin.
                if (options.complete) {
                    switch (contentType) {
                        // JSON
                        case "application/json":
                            options.complete.call (xhr, JSON.parse(xhr.responseText) );
                            break;
                        // XML
                        case "text/xml":
                        case "application/xml":
                            options.complete.call( xhr, xhr.responseXML );
                            break;
                        // Txt
                        default:
                            options.complete.call( xhr, xhr.responseText );
                            break;
                    }
                }
            }
        };

        // Opnar requestið. Setur inn method, url og async vs sync
        xhr.open(options.method, url, true);

        // Skilar til baka objectinu tilbúið til að senda
        return xhr;
    } else {
        return false;
    }
};

// Lokafrágangur áður en ajax er sent.
App.ajax = function(url, options) {
    // Notað aðferðina hér fyrir ofan.
    var xhr = App.createXHR(url, options);

    if (xhr) {
        // Headerinn settur með.
        xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

        // Ef POST request þá þarf að bæta við öðrum header upp á hvernig gögnin
        // koma.
        if (options.method.toUpperCase() == "POST") {
            xhr.setRequestHeader('Content-Type', 'Application/x-www-form-urlencoded');
        }

        // ajaxið sent.
        xhr.send(options.data);
    }
};
