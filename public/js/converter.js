var Converter;

(function() {
'use strict';

var s;
/**
 * Consturctor for the Converter application
 * @param {object} args Object containing every 
 *                      thing that needs so the application can run
 */
Converter = function( args ) {
    this.settings = {};
    this.settings.body = args.body;
    this.settings.container = args.container;
    this.settings.search = args.search || null;
    this.settings.provider = args.provider || 'm5';
    this.settings.templates = args.templates;

    s = this.settings;

    this.getData(s.provider ,function(response) {
        this.renderMenu(JSON.parse(response).results);
    }.bind(this));
};

/**
 * Renders the main menu of the application
 * @param  {object} data object containing all the data
 * @return {mixed}
 */
Converter.prototype.renderMenu = function(data) {
    s.container.innerHTML = '';   
    history.pushState(null, null, '/');
    if (data.length) {
        data.forEach(function(currency) {
            var template = this.renderTemplate(s.templates.currency.innerHTML, currency);
            s.container.innerHTML += template;
        }.bind(this));
        this.addEvents();
    } else {
        s.container.innerHTML += this.renderTemplate(s.templates.error, { message: 'Sorry, no results matched your search' });
    }
};

/**
 * Renders the converter view
 * @param  {object} currency object containing allt the data for this currency
 * @return {mixed}
 */
Converter.prototype.renderConverter = function(currency) {
    s.container.innerHTML = this.renderTemplate( s.templates.converter.dom, currency[0] );  

    history.pushState(null, null, currency[0].shortName);

    var back = document.getElementById('back');
    if (back !== null) {
        back.addEventListener('click', this.events.back);
    }

    var input = document.getElementById(s.templates.converter.inputId);
    input.addEventListener('keypress', this.events.binding);
};

/**
 * Adds the events for the main menu (click and search)
 */
Converter.prototype.addEvents = function() {
    for (var i = 0; i < s.container.children.length; i++) {
        s.container.children[i].addEventListener('click', this.events.click);
    }

    if (s.search !== null) {
        s.search.addEventListener('input', this.events.search);
    }
};

/**
 * Searches the data for currency shortName and longName
 * @param  {string} search What are you looking for
 * @return {object}        Object for this currency
 */
Converter.prototype.findCurrency = function( search ) {
    var data = localStorage.getItem('data');
    data = JSON.parse(data);
    data = data.results;

    return data.filter(function(currency) {
        var re = new RegExp(search.toLowerCase());

        if (re.test(currency.shortName.toLowerCase()) || re.test(currency.longName.toLowerCase())) {
            return true;
        }
    });
};

/**
 * Controller methods for the views
 * @type {Object}
 */
Converter.prototype.events = {

    /**
     * What should happend when the user clicks on currency on the main view
     * @param  {event} e 
     * @return {view}   
     */
    click: function (e) {
        var currency = Converter.prototype.findCurrency(this.childNodes[0].data.trim().toLowerCase());
        Converter.prototype.renderConverter(currency);
        e.preventDefault();
    },

    /**
     * Calculates the value for the currency in relation to ISK
     * and outputs it to the input with the corresponded id form the settings
     * @param  {event} key 
     * @return {integer}
     */
    binding: function (key) {
        if ((key.keyCode >= 48 && key.keyCode <= 57) || (key.keyCode >= 96 && key.keyCode <= 105) && key.keyCode == 8 && key.keyCode == 46) {
            var output = document.getElementById(s.templates.converter.outputId);
            var value = output.dataset.value;
            output.value = ((this.value + String.fromCharCode(key.keyCode)) * value).toFixed(0) + 'kr.';
        } else {
            key.preventDefault();
        }        
    },

    /**
     * Goes back to the main view
     * @param  {event} e 
     * @return {view}
     */
    back: function (e) {
        Converter.prototype.renderMenu((JSON.parse(localStorage.getItem('data'))).results);
        history.pushState(null, null, '/');
        e.preventDefault();
    },

    /**
     * Searches for currency  
     * @param  {String} input 
     * @return {view}   returns the view for the currency converter
     */
    search: function (input) {
        var value = input.path[0].value;
        var data = Converter.prototype.findCurrency(value);
        if (data.length === 1) {
            Converter.prototype.renderConverter(data);
        } else {
            Converter.prototype.renderMenu(data);
        }
    }
};

/**
 * Render the view with handlebar.js
 * @param  {String} el   string of the template
 * @param  {Object} data Template data
 * @return {DOM}      
 */
Converter.prototype.renderTemplate = function( el, data ) {
    return Handlebars.compile(el)(data);
};

/**
 * Gets the data from apis.is
 * @param  {String}   provider Which provider should be used ['m5', 'arion', 'lb']
 * @param  {Function} callback 
 * @return {Object} 
 */
Converter.prototype.getData = function (provider, callback) {
    App.ajax('http://apis.is/currency/' + provider, {
        method: 'GET',
        complete: function(response) {
            callback(response);
            localStorage.setItem('data', response);
        }
    }); 
};
})();