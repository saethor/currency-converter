(function() {
'use strict';
var converter = new Converter({
    body: document.getElementsByTagName('body')[0],
    container: document.getElementById('currencies'),
    search: document.getElementById('search'), 
    provider: 'lb', 

    templates: {
        currency: document.getElementById('currency'),
        error: document.getElementById('error').innerHTML,
        converter: { 
            dom: document.getElementById('converter').innerHTML,
            inputId: 'input',
            outputId: 'output'
        },
        errorPopup: document.getElementById('error-popup').innerHTML
    }
});

Handlebars.registerHelper('lowerCase', function(string) {
    return string.toLowerCase();
});

window.onpopstate = function() {
    if (document.location.pathname === '/') {
        Converter.prototype.renderMenu((JSON.parse(localStorage.getItem('data')).results));
    }
};

// if (document.location.pathname !== '/') {
//     var data = (JSON.parse(localStorage.getItem('data'))).results;
//     data.forEach(function(currency) {
//         if (document.location.pathname === '/' + currency.shortName) {
//             Converter.prototype.renderConverter(currency);
//             console.log(true);
//         }
//     });
// }




})();